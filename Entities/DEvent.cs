﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Event.Domain.Entities
{
    public class DEvent
    {
        public DEvent()
        {
            Categories = new HashSet<DCategory>();
            Albums = new HashSet<DAlbum>();
        }
        public Guid Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string Content { get; set; }
        public string Address { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CoverImg { get; set; }

        public virtual ICollection<DCategory> Categories { get; set; }
        public virtual ICollection<DAlbum> Albums { get; set; }

        public Guid? SourceId { get; set; }
        [ForeignKey(nameof(SourceId))]
        public virtual DSource Source { get; set; }
    }
}
