﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Event.Domain.Entities
{
    public class DAlbum
    {
        public DAlbum()
        {
            Images = new HashSet<DImages>();
        }
        public Guid Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<DImages> Images { get; set; }


        public Guid EventId { get; set; }
        [ForeignKey(nameof(EventId))]
        public virtual DEvent Event { get; set; }
    }
}
